#The startup of fish to include these instances

#Disable greeting when fish is launched.
set fish_greeting  

#Display the neofetch program on startup.
 neofetch           
# alsi
# sfetch
# hfetch

#Use the alias keyword, which is essentially a shell wrapper 
#for the function syntax above. When using this syntax, the $argv 
#arguments string will be appended automatically.

# requires exa
alias la="exa --icons -las type"
alias lr="exa --icons -1Rs type"
alias lt="exa --icons -1Ts type"
alias ls="exa --icons -l"
alias l="exa --icons -1s type"
alias ll="exa --icons -as type"

alias open="xdg-open"
alias pdf="qpdf --decrypt"
alias df="df -h"
alias :wq="exit"

#pacman updates
alias syyuu="sudo pacman -Syyuu"
alias syu="sudo pacman -Syu"
alias mirror="sudo pacman -Syyu"
alias cache="sudo paccache -rk 1"
alias autoclean="sudo pacman -Sc"
alias s="sudo pacman -S"
alias r="sudo pacman -R"
alias rcns="sudo pacman -Rcns"
alias rns="yay -Rns"
alias aur="pacseek"
alias archkey="sudo pacman -Sy archlinux-keyring"
alias arcokey="sudo pacman -Sy arcolinux-keyring"

#misc
alias d="dict"
alias mpv="devour mpv"



